# Challenge 06 API Contract

## API Contract (13 Mei 2022)

### POST /auth/login

#### Success

- Request

```json
{
  "username": "irawan",
  "password": "12345"
}
```

- Response 200

```json
{
  "token": "${string}"
}
```

#### Wrong Password

- Request

```json
{
  "username": "irawan",
  "password": "Qweqwe"
}
```

- Response (401)

```json
{
  "message": "Invalid username or password"
}
```

#### Wrong username

- Request

```json
{
  "username": "wawan",
  "password": "12345"
}
```

- Response (401)

```json
{
  "message": "Invalid username or password"
}
```

#### No Request

- Request

```json
{}
```

- Response (500)

```json
{
  "message": "Internal Server Error"
}
```

### POST /auth/register

#### Success

- Request

```json
{
  "username": "irawan",
  "password": "12345"
}
```

- Response 200

```json
{
  "message": "Register Succesfully"
}
```

#### No Password

- Request

```json
{
  "username": "irawan"
}
```

- Response (500)

```json
{
  "message": "Internal Server Error"
}
```

#### Wrong username

- Request

```json
{
  "password": "12345"
}
```

- Response (500)

```json
{
  "message": "Internal Server Error"
}
```

#### No Request

- Request

```json
{}
```

- Response (500)

```json
{
  "message": "Internal Server Error"
}
```

### GET /usergames

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Response

```json
{
  "username": "irawan",
  "password": "${string}"
}
```

#### No Auth Token

- Headers

```json
{}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid Token

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

### GET /usergames

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Response

```json
{
  "username": "irawan",
  "password": "${string}"
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid auth token

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

### POST /usergames

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "username": "irawan",
  "password": "12345"
}
```

- Response

```json
{
  "message": "Succesfully create User Games"
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Body

```json
{
  "username": "irawan",
  "password": "12345"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid auth token

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Body

```json
{
  "username": "irawan",
  "password": "12345"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Required Field Violation

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{}
```

- Response

```json
{
  "message": ["Username is required", "Password is required"]
}
```

### PUT /usergames/1

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "username": "irawan",
  "password": "op12345"
}
```

- Response

```json
{
  "message": "Successfully update User Games"
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Body

```json
{
  "username": "irawan",
  "password": "op12345"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid auth token

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Body

```json
{
  "username": "irawan",
  "password": "op12345"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Data Id Not Found

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "username": "irawan",
  "password": "op12345"
}
```

- Response

```json
{
  "message": "Data Not Found"
}
```

### GET /biodatas

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Response

```json
{
  "id": 9,
  "fullname": "Dedi Irawan",
  "age": 19,
  "gender": "male",
  "level": 9,
  "userId": 21
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid Token biodatas

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Response

```json
{
  "message": "Unauthorized request"
}
```

#### Biodatas Not Found

- Headers

```json
{
  "authorization": "${string}"
}
```

- Response (404)

```json
{
  "message": "User Game Biodatas not found"
}
```

### POST /biodatas

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "fullname": "Dedi Irawan",
  "age": 19,
  "gender": "male",
  "level": 9
}
```

- Response (200)

```json
{
  "message": "Succesfully create User Game Biodatas"
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Body

```json
{
  "fullname": "Dedi Irawan",
  "age": 19,
  "gender": "male",
  "level": 9
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid auth token

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Body

```json
{
  "fullname": "Dedi Irawan",
  "age": 19,
  "gender": "male",
  "level": 9
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Biodatas Internal Server Error - Username Unique

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "fullname": "Dedi Irawan",
  "age": 19,
  "gender": "male",
  "level": 9
}
```

- Response (500)

```json
{
  "message": "Internal Server Error"
}
```

#### Biodatas Required Request

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{}
```

- Response (400)

```json
{
  "message": [
    "Fullname is required",
    "Age is required",
    "Gender is required",
    "Level is required"
  ]
}
```

### PUT /biodatas

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "fullname": "Dedi Irawan",
  "age": 11,
  "gender": "male",
  "level": 12
}
```

- Response (200)

```json
{
  "message": "Successfully update User Game Biodatas"
}
```

#### Unautorized Biodatas

- Headers

```json
{}
```

- Body

```json
{
  "fullname": "Dedi Irawan",
  "age": 11,
  "gender": "male",
  "level": 12
}
```

- Response (200)

```json
{
  "message": "Unauthorized request"
}
```

#### Biodatas - Invalid Token

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Body

```json
{
  "fullname": "Irawan Hermawan",
  "age": 11,
  "gender": "male",
  "level": 12
}
```

- Response (200)

```json
{
  "message": "Unauthorized request"
}
```

### Delete /biodatas

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Response (200)

```json
{
  "message": "Successfully delete User Game Biodatas"
}
```

#### Unautorized Biodatas

- Headers

```json
{}
```

- Response (200)

```json
{
  "message": "Unauthorized request"
}
```

#### Biodatas - Invalid Token

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Response (200)

```json
{
  "message": "Unauthorized request"
}
```

### GET /Histories

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Response

```json
{
  "id": 16,
  "time": "00:34:54",
  "score": 9,
  "description": "Lorem Ipsum",
  "userId": 21
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid Token Histories

- Headers

```json
{
  "authorization": "papabolobolo"
}
```

- Response

```json
{
  "message": "Unauthorized request"
}
```
