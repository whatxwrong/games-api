const express = require('express')
const app = express()
const port = 3000
const routes = require('./routes/index.route')
const errorHandler = require('./errorHandler')
const morgan = require('morgan')
require('dotenv').config()

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(morgan('dev'))

app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));

app.use(express.static('public'))
app.use(routes)
app.use(errorHandler)

module.exports = app
