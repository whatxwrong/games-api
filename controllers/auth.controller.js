'use strict';
const { UserGames } = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const otpGenerator = require('otp-generator')
const { OAuth2Client } = require('google-auth-library')
const axios = require('axios')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)
const sendEmail = require('../helpers/sendEmail.helper')

class AuthController {
  // Login with password
  static async login(req, res, next) {
    try {
      const userGames = await UserGames.findOne({
        where: {
          email: req.body.email
        }
      })
      
      if (!userGames) {
        throw {
          status: 401,
          message: 'Invalid email or password'
        }
      }
      if (bcrypt.compareSync(req.body.password, userGames.password)) {
        const token = jwt.sign({
          id: userGames.id,
          email: userGames.email
        }, 'qweqwe')
        
        res.status(200).json({
          token
        })
      } else {
        throw {
          status: 401,
          message: 'Invalid email or password'
        }
      }
    } catch (err) {
      next(err)
    }

  }

  // Login with Google
  static async loginGoogle(req, res, next) {
    try {
      // melakukan verifikasi id token
      const payload = await client.verifyIdToken({
        idToken: req.body.google_id_token,
        audience: process.env.GOOGLE_CLIENT_IDs
      })
      
      console.log(payload.payload.email)
      // mencari email dari google di database
      const user = await UserGames.findOne({
        where: {
          email: payload.payload.email
        }
      })
      
      if (user) {
        const token = jwt.sign({
          id: user.id,
          email: user.email
        }, process.env.MY_JWT_TOKEN)
        res.status(200).json({ token })
      } else {
        const createdUser = await UserGames.create({
          email: payload.payload.email
        })

        // Mengirim email register
        const html = `
        <pre>
        Hi ${createdUser.email},
        Welcome to What Games
        </pre>
        `
        await sendEmail('What Games | what@games.id', createdUser.email, html, null, 'Register User')

        const token = jwt.sign({
          id: createdUser.id,
          email: createdUser.email
        }, process.env.MY_JWT_TOKEN)
        res.status(200).json({ token })
      }
    } catch (err) {
      next()
    }
  } 
  
  // Login with Facebook
  static async loginFacebook(req, res, next) {
    try {
      const response = await axios.get(`https://graph.facebook.com/v13.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${req.body.facebook_id_token}`)
      console.log(response.data)
      // mencari user di database
      const user = await UserGames.findOne({
        where: {
          email: response.data.email
        }
      })

      if (user) {
        const token = jwt.sign({
          id: user.id,
          email: user.email
        }, process.env.MY_JWT_TOKEN)
        res.status(200).json({ token })
      } else {
        const createdUser = await UserGames.create({
          email: response.data.email
        })

        // Mengirim email register
        const html = `
        <pre>
        Hi ${createdUser.email},
        Welcome to What Games
        </pre>
        `
        await sendEmail('What Games | what@games.id', createdUser.email, html, null, 'Register User')

        const token = jwt.sign({
          id: createdUser.id,
          email: createdUser.email
        }, process.env.MY_JWT_TOKEN)
        res.status(200).json({ token })
      }
    } catch (err) {
      next()
    }
  } 

  static async register(req, res, next) {
    try {
      if (req.file) {
        req.body.video = `http://localhost:3000/${req.file.filename}`
      }
      await UserGames.create({
        email: req.body.email,
        password: req.body.password,
        video: req.body.video
      })

      // Mengirim Email Register
      const html = `
      <pre>
      Hi ${req.body.email},
      Welcome to What Games
      </pre>
      `
      await sendEmail('What Games | what@games.id', req.body.email, html, null, 'Register User')
      res.status(201).json({
        message: 'Register Succesfully'
      })
    } catch (err) {
      next(err)
    }
  }

  static async sendForgotPasswordToken(req, res, next) {
    // user mengirimkan alamat email
    // cek apakah email terdaftar di aplikasi
    // kita generate token (OTP) => pakai otp-generator
    // simpan otp ke database, dan kita tentukan expirednya
    // kirim email

    try {
      const userGames = await UserGames.findOne({
        where: {
          email: req.body.email
        }
      })

      if (!userGames) {
        throw {
          status: 400,
          message: 'Invalid email'
        }
      } else {
        const otp = otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false });
        const salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(otp, salt)
        const expiredDate = new Date(new Date().getTime() + 5 * 60000)
        await UserGames.update({
          forgot_pass_token: hash,
          forgot_pass_token_expired_at: expiredDate
        }, {
          where: {
            email: req.body.email
          }
        })
        const html = `
        <pre>
        Token Anda: ${otp}
        Token Kedaluarsa pada ${expiredDate}
        </pre>
        `
        await sendEmail('What Games | what@games.id', req.body.email, html, null, 'Your Forgot Password Token')
        res.status(200).json({
          message: 'Succesfully send email'
        })
        
      }
    } catch(err) {
      next(err)
    }
  }

  static async verifyForgotPasswordToken(req, res, next) {
    try {
      const userGames = await UserGames.findOne({
        where: {
          email: req.body.email
        }
      })
      console.log(userGames)
      if (bcrypt.compareSync(req.body.token, userGames.forgot_pass_token)) {
        if (userGames.forgot_pass_token_expired_at > new Date) {
          res.status(200).json({
            valid: true,
            message: 'Token is valid'
          })
        } else {
          throw {
            status: 400,
            message: 'Invalid token'
          }
        }
      } else {
        throw {
          status: 400,
          message: 'Invalid token'
        }
      }
    } catch(err) {
      next(err)
    }
  }

  static async changePassword(req, res, next) {
    // password dan password confirmation => BE dan FE
    // FE juga harus mengirimkan email dan token
    try {
      if (req.body.password === req.body.password_confirmation) {
        const userGames = await UserGames.findOne({
          where: {
            email: req.body.email
          }
        })
        if (userGames) {
          if (bcrypt.compareSync(req.body.token, userGames.forgot_pass_token)) {
            if (userGames.forgot_pass_token_expired_at > new Date()) {
              const salt = bcrypt.genSaltSync(10)
              const hash = bcrypt.hashSync(req.body.password, salt)
              await UserGames.update({
                password: hash,
                forgot_pass_token: null,
                forgot_pass_token_expired_at: null
              }, {
                where: {
                  email: req.body.email
                }
              })
              res.status(200).json({
                message: 'Successfully change password'
              })
            } else {
              throw {
                status: 400,
                message: 'Invalid userGames or token'
              }
            }
          } else {
            throw {
              status: 400,
              message: 'Invalid userGames or token'
            }
          }
        } else {
          throw {
            status: 400,
            message: 'Invalid userGames or token'
          }
        }
      } else {
        throw {
          status: 400,
          message: 'Password does not match password confirmation'
        }
      }
    } catch(err) {
      next(err)
    }
  }
}

module.exports = AuthController