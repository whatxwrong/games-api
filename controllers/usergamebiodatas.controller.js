'use strict';
const { UserGameBiodatas, UserGames } = require('../models')

class UserGameBiodatasController {
  static async getById(req, res, next) {
    try {
      const userGameBiodatas = await UserGameBiodatas.findOne({
        where: {
          userId: req.user.id
        }
      })

      if (!userGameBiodatas) {
        throw {
          status: 404,
          message: 'User Game Biodatas not found'
        }
      } else {
        if (userGameBiodatas.userId !== req.user.id) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          res.status(200).json({
            id: userGameBiodatas.id,
            fullname: userGameBiodatas.fullname,
            age: userGameBiodatas.age,
            gender: userGameBiodatas.gender,
            level: userGameBiodatas.level,
            userId: userGameBiodatas.userId
          })
        }
      }

    } catch(err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
      await UserGameBiodatas.create({
        fullname: req.body.fullname,
        age: req.body.age,
        gender: req.body.gender,
        level: req.body.level,
        userId: req.user.id,
      })
      res.status(201).json({
        message: 'Succesfully create User Game Biodatas'
      })
    } catch (err) {
      next(err)
    }
  }

  static async update(req, res, next) {
    try {
      await UserGameBiodatas.update({
        fullname: req.body.fullname,
        age: req.body.age,
        gender: req.body.gender,
        level: req.body.level,
      }, {
        where: {
          // id: req.params.id,
          userId: req.user.id
        },
        returning: true,
      })
      res.status(200).json({
        message: 'Successfully update User Game Biodatas'
      })
    } catch(err) {
      next(err)
    }
  }

  static async delete(req, res, next) {
    try {
      const userGameBiodatas = await UserGameBiodatas.findOne({
        where: {
          userId: req.user.id
        }
      })

      if (!userGameBiodatas) {
        throw {
          status: 404,
          message: 'User Game Biodatas not found'
        }
      } else {
        if (userGameBiodatas.userId !== req.user.id) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          await UserGameBiodatas.destroy({
            where: {
              // id: req.params.id,
              id: req.user.id
            }
          })
          res.status(200).json({
            message: 'Successfully delete User Game Biodatas'
          })
        }
      }
    } catch(err) {
      next(err)
    }
  }

  // // Yang tidak digunakan

  // static async list(req, res, next) {
  //   // select * from UserGameBiodatas
  //   UserGameBiodatas.findAll({
  //     attributes: ['id', 'fullname', 'age', 'gender', 'level', 'userId'],
  //     include: {
  //       model: UserGames,
  //     }
  //   })
  //     .then((data) => {
  //       res.status(200).json(data)
  //     })
  //     .catch((error) => {
  //       res.status(500).json(error)
  //     })
  // }
  
  // static async searchByFullname(req, res, next) {
  //   // console.log(req.body.fullname);
  //   UserGameBiodatas.findAll({
  //     where: {
  //       fullname: {
  //           [Op.iLike]: '%'+req.body.fullname+'%'
  //       } 
  //     }
  //   })
  //     .then((data) => {
  //       res.status(200).json(data)
  //     })
  //     .catch((err) => {
  //       res.status(500).json(err)
  //     })
  // }

}

module.exports = UserGameBiodatasController