'use strict';
const { UserGameHistories, UserGames } = require('../models')

class UserGameHistoriesController {
  static async list(req, res, next) {
    try {
      const userGameBiodatas = await UserGameHistories.findAll({
        include: {
          model: UserGames,
        },
        where: {
          userId: req.user.id
        },
        attributes: ['id', 'time', 'score', 'description', 'userId']
      })
      res.status(200).json(userGameBiodatas)
    } catch(err) {
      next(err)
    }
  }
  
  static async getById(req, res, next) {
    try {
      const userGameHistories = await UserGameHistories.findOne({
        where: {
          id: req.params.id,
          userId: req.user.id
        },
        include: {
          model: UserGames,
        }
      })

      if (!userGameHistories) {
        throw {
          status: 404,
          message: 'User Games Histories not found'
        }
      } else {
        if (userGameHistories.userId !== req.user.id) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          res.status(200).json({
            id: userGameHistories.id,
            time: userGameHistories.time,
            score: userGameHistories.score,
            description: userGameHistories.description,
            userId: userGameHistories.userId
          })
        }
      }

    } catch(err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
      await UserGameHistories.create({
        time: req.body.time,
        score: req.body.score,
        description: req.body.description,
        userId: req.user.id,
      })
      res.status(201).json({
        message: 'Succesfully create User Game Histories'
      })
    } catch (err) {
      next(err)
    }
  }

  static async update(req, res, next) {
    try {
      await UserGameHistories.update({
        time: req.body.time,
        score: req.body.score,
        description: req.body.description,
      }, {
        where: {
          id: req.params.id,
          userId: req.user.id
        }
      })
      res.status(200).json({
        message: 'Successfully update User Game Histories'
      })
    } catch(err) {
      next(err)
    }
  }

  static async delete(req, res, next) {
    try {
      const userGameHistories = await UserGameHistories.findOne({
        where: {
          id: req.params.id,
          userId: req.user.id
        }
      })

      if (!userGameHistories) {
        throw {
          status: 404,
          message: 'User Game Histories not found'
        }
      } else {
        if (userGameHistories.userId !== req.user.id) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          await UserGameHistories.destroy({
            where: {
              id: req.params.id
            }
          })
          res.status(200).json({
            message: 'Successfully delete User Game Histories'
          })
        }
      }
    } catch(err) {
      next(err)
    }
  }
}

module.exports = UserGameHistoriesController