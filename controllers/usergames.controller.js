'use strict';
const { UserGames } = require('../models')

class UserGamesController {
  static async getById(req, res, next) {
    try {
      const userGames = await UserGames.findOne({
        where: {
          id: req.user.id
        }
      })

      if (!userGames) {
        throw {
          status: 404,
          message: 'User Games not found'
        }
      } else {
        if (userGames.id !== req.user.id) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          res.status(200).json({
            email: userGames.email,
            password: userGames.password
          })
        }
      }

    } catch (err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
      await UserGames.create({
        email: req.body.email,
        password: req.body.password
      })

      res.status(201).json({
        message: 'Succesfully create User Games'
      })
    } catch (err) {
      next(err)
    }
    
  }

  static async update(req, res, next) {
    try {
      const userGames = await UserGames.update({
        email: req.body.email,
        password: req.body.password
      }, {
        where: {
          // id: req.user.id,
          id: req.params.id
        }
      })
      
      if (userGames > 0) {
        res.status(200).json({
          message: 'Successfully update User Games'
        })
      } else {
        res.status(401).json({
          message: 'Data Not Found'
        })
      }
    } catch(err) {
      next(err)
    }

  }

}

module.exports = UserGamesController