'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameBiodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGameBiodatas.belongsTo(models.UserGames, { foreignKey: 'userId' })
    }
  }
  UserGameBiodatas.init({
    fullname: DataTypes.STRING,
    age: DataTypes.INTEGER,
    gender: DataTypes.ENUM('male', 'female'),
    level: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserGameBiodatas',
  });
  return UserGameBiodatas;
};