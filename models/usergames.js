'use strict';
const bcrypt = require('bcryptjs');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGames extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGames.hasOne(models.UserGameBiodatas, { foreignKey: 'userId', as: 'Biodata' })
      UserGames.hasMany(models.UserGameHistories, { foreignKey: 'userId', as: 'Histories' })
    }
  }
  UserGames.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    video: DataTypes.STRING,
    forgot_pass_token: DataTypes.STRING,
    forgot_pass_token_expired_at: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'UserGames',
  });
  UserGames.addHook('beforeCreate', (users, options) => {
    if (users.password) {
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(users.password, salt);
      users.password = hash;
    } else {
      users.password = '';
    }
  });
  return UserGames;
};