const express = require('express')
const router = express.Router()
const AuthController = require('../controllers/auth.controller')
const multer = require('multer')
const storage = require('../helpers/multerstorage.helper')
const upload = multer({
  storage,
  fileFilter: (req, file, cb) => {
    console.log(file)
    console.log(file.mimetype)
    if (file.mimetype === 'video/mp4' || file.mimetype === 'video/x-matroska' || file.mimetype === 'video/quicktime') {
      cb(null, true)
    } else {
      cb({
        status: 400,
        message: 'File type does not match'
      }, false)
    }
  }
})

// Login With Password
router.post('/login-password', 
  (req, res, next) => {
    const errors = []
    if (!req.body.email) {
      errors.push('Email is required')
    }
    if (!req.body.password) {
      errors.push('Password is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.login)

// Login With Google
router.post('/login-google',
  (req, res, next) => {
    const errors = []
    if (!req.body.google_id_token) {
      errors.push('google_id_token is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.loginGoogle)

// Login With Facebook
router.post('/login-facebook', 
  (req, res, next) => {
    const errors = []
    if (!req.body.facebook_id_token) {
      errors.push('facebook_id_token is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.loginFacebook)

// Register
router.post('/register', upload.single('video'),
  (req, res, next) => {
    const errors = []
    if (!req.body.email) {
      errors.push('Email is required')
    }
    if (!req.body.password) {
      errors.push('Password is required')
    }

    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.register)

// Mengirimkan Token Forgot Password Ke Email 
router.post('/send-forgot-pass-token', 
  (req, res, next) => {
    const errors = []
    if (!req.body.email) {
      errors.push('Email is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.sendForgotPasswordToken)

// Memverifikasi Token Forgot Password
router.post('/verify-forgot-pass-token', 
  (req, res, next) => {
    const errors = []
    if (!req.body.token) {
      errors.push('Token is required')
    }
    if (!req.body.email) {
      errors.push('Email is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.verifyForgotPasswordToken)

// Mengubah Password dan juga mengirimkan token  
router.post('/change-pass', 
  (req, res, next) => {
    const errors = []
    if (!req.body.token) {
      errors.push('Token is required')
    }
    if (!req.body.email) {
      errors.push('Email is required')
    }
    if (!req.body.password) {
      errors.push('password is required')
    }
    if (!req.body.password_confirmation) {
      errors.push('Password Confirmation  is required')
    }

    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.changePassword)

module.exports = router