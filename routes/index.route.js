const express = require('express')
const router = express.Router()
const authRoutes = require('./auth.route')
const usergamesRoute = require('./usergames.route')
const usergamebiodatasRoute = require('./usergamebiodatas.route')
const usergamehistoriesRoute = require('./usergamehistories.route')

router.use('/auth', authRoutes)
router.use('/usergames', usergamesRoute)
router.use('/biodatas', usergamebiodatasRoute)
router.use('/histories', usergamehistoriesRoute)

module.exports = router