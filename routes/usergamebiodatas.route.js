const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const UserGameBiodatas = require('../controllers/usergamebiodatas.controller')

router.post('/',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
(req, res, next) => {
  const errors = []
  if (!req.body.fullname) {
    errors.push('Fullname is required')
  }
  if (!req.body.age) {
    errors.push('Age is required')
  }
  if (!req.body.gender) {
    errors.push('Gender is required')
  }
  if (!req.body.level) {
    errors.push('Level is required')
  }

  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
},
UserGameBiodatas.create
)

router.get('/',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
UserGameBiodatas.getById
)

router.put('/',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
(req, res, next) => {
  const errors = []
  if (req.body.fullname !== undefined) {
    if (!req.body.fullname) {
      errors.push('Fullname is required')
    }
  }
  if (req.body.age !== undefined) {
    if (!req.body.age) {
      errors.push('Age is required')
    }
  }
  if (req.body.gender !== undefined) {
    if (!req.body.gender) {
      errors.push('Gender is required')
    }
  }
  if (req.body.level !== undefined) {
    if (!req.body.level) {
      errors.push('Level is required')
    }
  }

  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
},
UserGameBiodatas.update
)

router.delete('/',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
UserGameBiodatas.delete
)
module.exports = router