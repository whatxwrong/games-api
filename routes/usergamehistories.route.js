const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const UserGameHistories = require('../controllers/usergamehistories.controller')

router.post('/',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
(req, res, next) => {
  const errors = []
  if (!req.body.time) {
    errors.push('Time is required')
  }
  if (!req.body.score) {
    errors.push('Score is required')
  }
  if (!req.body.description) {
    errors.push('Description is required')
  }

  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
},
UserGameHistories.create
)

router.get('/', 
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
UserGameHistories.list
)

router.get('/:id',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
UserGameHistories.getById
)

router.put('/:id',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
(req, res, next) => {
  const errors = []
  if (req.body.time !== undefined) {
    if (!req.body.time) {
      errors.push('Time is required')
    }
  }
  if (req.body.score !== undefined) {
    if (!req.body.score) {
      errors.push('Score is required')
    }
  }
  if (req.body.description !== undefined) {
    if (!req.body.description) {
      errors.push('Description is required')
    }
  }

  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
},
UserGameHistories.update
)

router.delete('/:id',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
UserGameHistories.delete
)
module.exports = router