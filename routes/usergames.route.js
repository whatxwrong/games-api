const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const UserGamesController = require('../controllers/usergames.controller')

router.post('/',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
(req, res, next) => {
  const errors = []
  if (!req.body.email) {
    errors.push('email is required')
  }
  if (!req.body.password) {
    errors.push('Password is required')
  }

  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
},
UserGamesController.create
)

router.get('/',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
UserGamesController.getById
)

router.put('/:id',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
(req, res, next) => {
  const errors = []
  if (req.body.email !== undefined) {
    if (!req.body.email) {
      errors.push('email is required')
    }
  }
  if (req.body.password !== undefined) {
    if (!req.body.password) {
      errors.push('Password is required')
    }
  }

  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
},
UserGamesController.update
)


module.exports = router