'use strict';
const fs = require('fs');
const bcrypt = require('bcryptjs');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const data = JSON.parse(fs.readFileSync('./data.json', 'utf-8'))
   const userGames = data['UserGames'].map((element) => {
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(element.password, salt);
      return {
        email: element.email,
        password: hash,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    })
    
    return queryInterface.bulkInsert('UserGames', userGames);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('UserGames', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
