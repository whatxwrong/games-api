'use strict';
const fs = require('fs');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const data = JSON.parse(fs.readFileSync('./data.json', 'utf-8'))
    const userGameBiodatas = data['UserGameBiodatas'].map((element) => {
      return {
        fullname: element.fullname,
        age: element.age,
        gender: element.gender,
        level: element.level,
        userId: element.userId,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    })
    
    return queryInterface.bulkInsert('UserGameBiodatas', userGameBiodatas);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('userGameBiodatas', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
