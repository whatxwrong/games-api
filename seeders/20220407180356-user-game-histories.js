'use strict';
const fs = require('fs');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const data = JSON.parse(fs.readFileSync('./data.json', 'utf-8'))
    const userGameHistories = data['UserGameHistories'].map((element) => {
      return {
        time: element.time,
        score: element.score,
        description: element.description,
        userId: element.userId,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    })
    
    return queryInterface.bulkInsert('UserGameHistories', userGameHistories);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('userGameHistories', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
