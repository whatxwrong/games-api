'use strict'
const fs = require('fs')

const data = JSON.parse(fs.readFileSync('./data.json', 'utf-8'))
const userGames = data['UserGames'].map((element) => {
    return {
        username: element.username,
        password: element.password,
        createdAt: new Date(),
        updatedAt: new Date(),
    }
});
const userGameBiodatas = data['UserGameBiodatas'].map((element) => {
    return {
        fullname: element.fullname,
        age: element.age,
        gender: element.gender,
        level: element.level,
        userId: element.userId,
        createdAt: new Date(),
        updatedAt: new Date(),
    }
});
const userGameHistories = data['UserGameHistories'].map((element) => {
    return {
        time: element.time,
        score: element.score,
        description: element.description,
        userId: element.userId,
        createdAt: new Date(),
        updatedAt: new Date(),
    }
});

console.log(userGames);
console.log(userGameBiodatas);
console.log(userGameHistories);