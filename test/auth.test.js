const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const bcrypt = require('bcryptjs')
const app = require('../app')

beforeEach(async () => {
  // memasukkan data dummy ke database testing
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync("12345", salt)
  await queryInterface.bulkInsert('UserGames', [
    {
      email: "irawan@mail.com",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])
})

afterEach(async () => {
  await queryInterface.bulkDelete('UserGames', {}, { truncate: true, restartIdentity: true, cascade: true })
})

describe('Login API', () => {
  it('Success', (done) => {
    request(app)
    .post('/auth/login')
    .send({
      email: "irawan@mail.com",
      password: "12345"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('token')
        done()
      }
    })
  })

  it('Wrong password', (done) => {
    request(app)
    .post('/auth/login')
    .send({
      email: "irawan@mail.com",
      password: "qweqwe"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Invalid email or password')
        done()
      }
    })
  })
  
  it('Wrong email', (done) => {
    request(app)
    .post('/auth/login')
    .send({
      email: "wawan@mail.com",
      password: "12345"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Invalid email or password')
        done()
      }
    })
  })
})

describe('Register API', () => {
  it('Success', (done) => {
    request(app)
    .post('/auth/register')
    .send({
      email: "mawang@mail.com",
      password: "12345"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(201)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Register Succesfully')
        done()
      }
    })
  })

  it('No password', (done) => {
    request(app)
    .post('/auth/register')
    .send({
      email: "mawang@mail.com"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(500)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Internal Server Error')
        done()
      }
    })
  })
  
  it('No email', (done) => {
    request(app)
    .post('/auth/register')
    .send({
      password: "12345"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(500)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Internal Server Error')
        done()
      }
    })
  })
})