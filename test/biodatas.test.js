const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')
const app = require('../app')
// sebelum test, kita butuh data user
let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync("12345", salt)
  await queryInterface.bulkInsert('UserGames', [
    {
      username: "irawan",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])

  await queryInterface.bulkInsert('UserGameBiodatas', [
    {
      userId: 1,
      fullname: "Dedi Irawan",
      age: 19,
      gender: "male",
      level: 9,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])

  token = jwt.sign({
    id: 1,
    username: "irawan"
  }, 'qweqwe')

})

afterEach(async () => {
  await queryInterface.bulkDelete('UserGames', {}, { truncate: true, restartIdentity: true, cascade: true })
  await queryInterface.bulkDelete('UserGameBiodatas', {}, { truncate: true, restartIdentity: true, cascade: true })
})

describe('GET Biodatas', () => {
  it('Success', (done) => {
    request(app)
    .get('/biodatas')
    .set('authorization', token)
    .end((err, res) => {
    if (err) {
      done(err)
    } else {
      expect(res.status).toBe(200)
      expect(res.body).toHaveProperty('id')
      expect(res.body).toHaveProperty('fullname')
      expect(res.body).toHaveProperty('gender')
      expect(res.body).toHaveProperty('level')
      expect(res.body).toHaveProperty('userId')
      done()
    }
    })
  })
})

describe('DELETE Biodata', () => {
  it('Success', (done) => {
    request(app)
    .delete('/biodatas')
    .set("authorization", token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Successfully delete User Game Biodatas')
        done()
      }
    })
  })
})