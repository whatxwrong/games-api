const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')
const app = require('../app')
// sebelum test, kita butuh data user
let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync("12345", salt)
  await queryInterface.bulkInsert('UserGames', [
    {
      username: "irawan",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])

//   await queryInterface.bulkInsert('UserGameBiodatas', [
//     {
//       userId: 1,
//       fullname: "Dedi Irawan",
//       age: 19,
//       gender: "male",
//       level: 9,
//       createdAt: new Date(),
//       updatedAt: new Date()
//     }
//   ])

  token = jwt.sign({
    id: 1,
    username: "irawan"
  }, 'qweqwe')

})

afterEach(async () => {
  await queryInterface.bulkDelete('UserGames', {}, { truncate: true, restartIdentity: true, cascade: true })
  await queryInterface.bulkDelete('UserGameBiodatas', {}, { truncate: true, restartIdentity: true, cascade: true })
})

describe('GET Biodata', () => {
  it('no auth', (done) => {
    request(app)
    .get('/biodatas')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('invalid token', (done) => {
    request(app)
    .get('/biodatas')
    .set('authorization', 'papabolobolo')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Not Found', (done) => {
    request(app)
    .get('/biodatas')
    .set('authorization', token)
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(404)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('User Game Biodatas not found')
        done()
      }
    })
  })
})

describe('POST User Game Biodatas', () => {
  it('Success', (done) => {
    request(app)
    .post('/biodatas')
    .set("authorization", token)
    .send({
      fullname: "Dedi Irawan",
      age: 19,
      gender: "male",
      level: 9
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(201)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Succesfully create User Game Biodatas')
        done()
      }
    })
  })
  it('No auth', (done) => {
    request(app)
    .post('/biodatas')
    .send({
      fullname: "Dedi Irawan",
      age: 19,
      gender: "male",
      level: 9
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Invalid auth token', (done) => {
    request(app)
    .post('/biodatas')
    .set("authorization", "papabolobolo")
    .send({
      fullname: "Dedi Irawan",
      age: 19,
      gender: "male",
      level: 9
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Required field violation', (done) => {
    request(app)
    .post('/biodatas')
    .set("authorization", token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(400)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message.length).toBe(4)
        expect(res.body.message.includes('Fullname is required')).toBe(true)
        expect(res.body.message.includes('Age is required')).toBe(true)
        expect(res.body.message.includes('Gender is required')).toBe(true)
        expect(res.body.message.includes('Level is required')).toBe(true)
        done()
      }
    })
  })
}) 

describe('Update User Game Biodatas', () => {
  it('Success', (done) => {
    request(app)
    .put('/biodatas')
    .set("authorization", token)
    .send({
      fullname: "Dedi Irawan",
      age: 11,
      gender: "male",
      level: 12
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Successfully update User Game Biodatas')
        done()
      }
    })
  })
  it('No auth', (done) => {
    request(app)
    .put('/biodatas')
    .send({
      fullname: "Dedi Irawan",
      age: 11,
      gender: "male",
      level: 12
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Invalid auth token', (done) => {
    request(app)
    .put('/biodatas')
    .set("authorization", "papabolobolo")
    .send({
      fullname: "Dedi Irawan",
      age: 11,
      gender: "male",
      level: 12
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})  

describe('Delete User Game Biodatas', () => {
//   it('Success', (done) => {
//     request(app)
//     .delete('/biodatas')
//     .set("authorization", token)
//     .end((err, res) => {
//       if (err) {
//         done(err)
//       } else {
//         expect(res.status).toBe(200)
//         expect(res.body).toHaveProperty('message')
//         expect(res.body.message).toBe('Successfully delete User Game Biodatas')
//         done()
//       }
//     })
//   })
  it('No auth', (done) => {
    request(app)
    .delete('/biodatas')
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Invalid auth token', (done) => {
    request(app)
    .delete('/biodatas')
    .set("authorization", "papabolobolo")
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})  

