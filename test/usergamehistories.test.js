const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')
const app = require('../app')
// sebelum test, kita butuh data user
let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync("12345", salt)
  await queryInterface.bulkInsert('UserGames', [
    {
      username: "irawan",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])

  await queryInterface.bulkInsert('UserGameHistories', [
    {
      userId: 1,
      time: "00:34:34",
      score: 19,
      description: "My Adventure",
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])

  token = jwt.sign({
    id: 1,
    username: "irawan"
  }, 'qweqwe')

})

afterEach(async () => {
  await queryInterface.bulkDelete('UserGames', {}, { truncate: true, restartIdentity: true, cascade: true })
  await queryInterface.bulkDelete('UserGameHistories', {}, { truncate: true, restartIdentity: true, cascade: true })
})

describe('GET Histories', () => {
  it('Success', (done) => {
    request(app)
    .get('/histories')
    .set('authorization', token)
    .end((err, res) => {
    if (err) {
      done(err)
    } else {
      expect(res.status).toBe(200)
      expect(Array.isArray(res.body)).toBe(true)
      done()
    }
    })
  })
  it('no auth', (done) => {
    request(app)
    .get('/histories')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('invalid token', (done) => {
    request(app)
    .get('/histories')
    .set('authorization', 'papabolobolo')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})

describe('GET ID Histories', () => {
  it('Success', (done) => {
    request(app)
    .get('/histories/1')
    .set('authorization', token)
    .end((err, res) => {
    if (err) {
      done(err)
    } else {
      expect(res.status).toBe(200)
      expect(res.body).toHaveProperty('id')
      expect(res.body).toHaveProperty('time')
      expect(res.body).toHaveProperty('score')
      expect(res.body).toHaveProperty('description')
      expect(res.body).toHaveProperty('userId')
      done()
    }
    })
  })
  it('no auth', (done) => {
    request(app)
    .get('/histories/1')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('invalid token', (done) => {
    request(app)
    .get('/histories/1')
    .set('authorization', 'papabolobolo')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Not Found', (done) => {
    request(app)
    .get('/histories/99')
    .set('authorization', token)
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(404)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('User Games Histories not found')
        done()
      }
    })
  })
})

describe('POST | Create Histories', () => {
  it('Success', (done) => {
    request(app)
    .post('/histories')
    .set('authorization', token)
    .send({
      time: "00:34:54",
      score: 9,
      description: "My Histories"
    })
    .end((err, res) => {
    if (err) {
      done(err)
    } else {
      expect(res.status).toBe(201)
      expect(res.body).toHaveProperty('message')
      expect(res.body.message).toBe('Succesfully create User Game Histories')
      done()
    }
    })
  })
  it('no auth', (done) => {
    request(app)
    .post('/histories')
    .send({
      time: "00:34:54",
      score: 9,
      description: "My Histories"
    })
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('invalid token', (done) => {
    request(app)
    .post('/histories')
    .set('authorization', 'papabolobolo')
    .send({
      time: "00:34:54",
      score: 9,
      description: "My Histories"
    })
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})

describe('PUT | Update Histories', () => {
  it('Success', (done) => {
    request(app)
    .put('/histories/1')
    .set('authorization', token)
    .send({
      time: "00:34:54",
      score: 9,
      description: "My Histories"
    })
    .end((err, res) => {
    if (err) {
      done(err)
    } else {
      expect(res.status).toBe(200)
      expect(res.body).toHaveProperty('message')
      expect(res.body.message).toBe('Successfully update User Game Histories')
      done()
    }
    })
  })
  it('no auth', (done) => {
    request(app)
    .put('/histories/1')
    .send({
      time: "00:34:54",
      score: 9,
      description: "My Histories"
    })
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('invalid token', (done) => {
    request(app)
    .put('/histories/1')
    .set('authorization', 'papabolobolo')
    .send({
      time: "00:34:54",
      score: 9,
      description: "My Histories"
    })
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})

describe('DELETE | Delete Histories', () => {
  it('Success', (done) => {
    request(app)
    .delete('/histories/1')
    .set('authorization', token)
    .end((err, res) => {
    if (err) {
      done(err)
    } else {
      expect(res.status).toBe(200)
      expect(res.body).toHaveProperty('message')
      expect(res.body.message).toBe('Successfully delete User Game Histories')
      done()
    }
    })
  })
  it('no auth', (done) => {
    request(app)
    .delete('/histories/1')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('invalid token', (done) => {
    request(app)
    .delete('/histories/1')
    .set('authorization', 'papabolobolo')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Not Found', (done) => {
    request(app)
    .delete('/histories/99')
    .set('authorization', token)
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(404)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('User Game Histories not found')
        done()
      }
    })
  })
})