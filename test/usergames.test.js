const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')
const app = require('../app')
// sebelum test, kita butuh data user
let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync("12345", salt)
  await queryInterface.bulkInsert('UserGames', [
    {
      email: "irawan@mail.com",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])

  token = jwt.sign({
    id: 1,
    email: "irawan@mail.com"
  }, 'qweqwe')

})

afterEach(async () => {
  await queryInterface.bulkDelete('UserGames', {}, { truncate: true, restartIdentity: true, cascade: true })
})


describe('POST User Games', () => {
  it('Success', (done) => {
    request(app)
    .post('/usergames')
    .set("authorization", token)
    .send({
      email: "dede",
      password: "911"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(201)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Succesfully create User Games')
        done()
      }
    })
  })
  it('No auth', (done) => {
    request(app)
    .post('/usergames')
    .send({
      email: "dede",
      password: "911"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Invalid auth token', (done) => {
    request(app)
    .post('/usergames')
    .set("authorization", "papabolobolo")
    .send({
      email: "dede",
      password: "911"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Required field violation', (done) => {
    request(app)
    .post('/usergames')
    .set("authorization", token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(400)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message.length).toBe(2)
        expect(res.body.message.includes('email is required')).toBe(true)
        expect(res.body.message.includes('Password is required')).toBe(true)
        done()
      }
    })
  })

})  

describe('GET User Games', () => {
  it('Success', (done) => {
    request(app)
    .get('/usergames')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('email')
        expect(res.body).toHaveProperty('password')
        done()
      }
    })
  })
  it('No Auth', (done) => {
    request(app)
    .get('/usergames')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Invalid Token', (done) => {
    request(app)
    .get('/usergames')
    .set('authorization', 'papabolobolo')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})

describe('UPDATE User Games By:id', () => {
  it('Success', (done) => {
    request(app)
    .put('/usergames/1')
    .set('authorization', token)
    .send({
      "email": "mawang@mail.com",
      "password": "911"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Successfully update User Games')
        done()
      }
    })
  })

  it('No Auth', (done) => {
    request(app)
    .put('/usergames/1')
    .send({
      "email": "irawan@mail.com",
      "password": "911"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })

  it('Invalid Token', (done) => {
    request(app)
    .put('/usergames/1')
    .set('authorization', 'papabolobolo')
    .send({
      "email": "irawan@mail.com",
      "password": "911"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })

  it('Id Not Found', (done) => {
    request(app)
    .put('/usergames/99')
    .set('authorization', token)
    .send({
      "email": "mawang@mail.com",
      "password": "911"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Data Not Found')
        done()
      }
    })
  })
})